package ru.remezov.basket;

import java.util.ArrayList;
import java.util.List;

public class ProductBasket implements Basket{
    List<Product> products = new ArrayList<>();

    @Override
    public void addProduct(String name, int quantity) {
        products.add(new Product(name, quantity));
    }

    @Override
    public void removeProduct(String name) {
        for (Product product: products){
            if (product.getName().equals(name)) {
                products.remove(product);
                return;
            }
        }
        System.out.println("This name is not found");
    }

    @Override
    public void updateProductQuantity(String name, int quantity) {
        for (Product product: products) {
            if (product.getName().equals(name)) {
                product.setQuantity(quantity);
                return;
            }
        }
        System.out.println("This name is not found");
    }

    @Override
    public void clear() {
        if (!products.isEmpty()) {
            products.clear();
        } else {
            System.out.println("The basket is empty.");
        }
    }

    @Override
    public List<String> getProducts() {
        List<String> names = new ArrayList<>();
        if (!products.isEmpty()) {
            for (Product product: products) {
                names.add(product.getName());
            }
        } else {
            System.out.println("The basket is empty.");
        }
        return names;
    }

    @Override
    public int getProductQuantity(String name) {
        for (Product product: products) {
            if (product.getName().equals(name)) {
                return product.getQuantity();
            }
        }
        System.out.println("This name is not found");
        return 0;
    }
}
