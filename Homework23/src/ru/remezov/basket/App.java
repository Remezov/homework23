package ru.remezov.basket;

public class App {

    public static void main(String[] args) {
        ProductBasket productBasket = new ProductBasket();
        productBasket.addProduct("Cola", 2);
        productBasket.addProduct("Pepsi", 1);
        productBasket.addProduct("Mirinda", 10);
        productBasket.addProduct("Fanta", 5);
        //productBasket.removeProduct("Mirinda");
        productBasket.updateProductQuantity("Fanta", 15);
        System.out.println(productBasket.getProductQuantity("Fanta"));
        //productBasket.clear();
        System.out.println(productBasket.getProducts());
    }
}
